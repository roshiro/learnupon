class ApplicationController < ActionController::Base

  # Handles all errors here and present user with an alert message.
  # Ideally we could handle different errors better here and show a more detailed message to the user.
  rescue_from StandardError, with: :handle_generic_error

  def handle_generic_error
    flash[:alert] = "Sorry, there was an unexpected error. Please notify the administrator."
    redirect_to root_path
  end

end
