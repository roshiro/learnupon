module Concerns::Sanitizer
  extend ActiveSupport::Concern

  included do
    before_validation :do_sanitize

    cattr_accessor :fields_to_sanitize

    # Macro for models to define which fields will be sanitized
    def self.sanitize_fields(*fields)
      self.fields_to_sanitize = fields
    end
  end

  # Method that loops through the fields and strips all tags from the string.
  # All HTML tags or script tags will be removed from the string.
  def do_sanitize
    self.fields_to_sanitize.each do |field|
      public_send("#{field}=", ActionController::Base.helpers.sanitize(public_send(field)))
    end
  end
end
