class User < ApplicationRecord
  include Concerns::Sanitizer

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # Sanitize fields to prevent malicious user input
  sanitize_fields :first_name, :last_name

  # Validates presence and length for first_name and last_name
  validates :first_name, presence: true, length: { minimum: 2, maximum: 50 }
  validates :last_name , presence: true, length: { minimum: 2, maximum: 50 }

  # Only update display_name when either first_name or last_name changed
  before_save :set_display_name, if: -> (user) { user.first_name_changed? || user.last_name_changed? }

  # Callbacks to clear cache everytime user is saved or deleted
  after_save :clear_cache
  after_destroy :clear_cache

  # Returns the cached version of User.all
  # If cache is empty, it performs User.all and updates the cache
  def self.all_cached
    Rails.cache.fetch("User.all") { self.all.to_a }
  end

  private

  def set_display_name
    self.display_name = "#{first_name} #{last_name}"
  end

  def clear_cache
    Rails.cache.delete('User.all')
  end

end
