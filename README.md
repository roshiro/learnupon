# Steps to setup this application

Ruby version: 2.3.1

Run the following commands:

__1 - Install dependencies__
```
$ bundle install
```

__2 - Install memcached__
```
$ brew install memcached
```

__3 - Run migrations__
```
$ bundle exec rails db:migrate
```

__4 - Run seed file__
```
$ bundle exec rails db:seed
```

__5 - Start memcached in a terminal session__
```
$ memcached
```

__6 - Start rails server in another terminal session__
```
$ bundle exec rails s
```

## Accessing the application

Go to the browser and access:
http://localhost:3000

__Credentials:__  
user: admin@example.com  
password: 12341234

## Assumptions
__Unit tests:__   
I assumed they were not necessary for this technical interview test because it was not specified in the document and also given the guidance of 2 hours, it would be very tight to build good tests for the classes involved here.  
However, if unit tests are necessary I would be happy to do so if I have a chance.

__User Sign up and "Forgot password" functionalities:__  
I did not implement them as I assumed it was not required.
